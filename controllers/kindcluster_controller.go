/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"github.com/erkanerol/cluster-api-provider-kind/cluster"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/tools/clientcmd"
	"reflect"
	capierrors "sigs.k8s.io/cluster-api/errors"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"strconv"
	"strings"
	"time"

	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"

	infrastructurev1alpha1 "github.com/erkanerol/cluster-api-provider-kind/api/v1alpha1"
)

const (
	ClusterFinalizer = "kindcluster.infrastructure.cluster.x-k8s.io/v1alpha1"
	kubeconfigSuffix = "-kubeconfig"
)

// KindClusterReconciler reconciles a KindCluster object
type KindClusterReconciler struct {
	client.Client
	Scheme   *runtime.Scheme
	Provider cluster.Provider
}

//+kubebuilder:rbac:groups=infrastructure.cluster.x-k8s.io,resources=kindclusters,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=infrastructure.cluster.x-k8s.io,resources=kindclusters/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=infrastructure.cluster.x-k8s.io,resources=kindclusters/finalizers,verbs=update
//+kubebuilder:rbac:groups="",resources=secrets,verbs=get;list;watch;create;update;patch;delete

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the KindCluster object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.10.0/pkg/reconcile
func (r *KindClusterReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	logger := log.FromContext(ctx)

	logger.V(1).Info("Reconciling KindCluster")
	var c infrastructurev1alpha1.KindCluster
	if err := r.Client.Get(context.TODO(), req.NamespacedName, &c); err != nil {
		return ctrl.Result{}, ignoreIfNotFound(err)
	}

	current := c.DeepCopy()

	defer func(kc *infrastructurev1alpha1.KindCluster) {
		backup := kc.DeepCopy()
		if !reflect.DeepEqual(current.Status, kc.Status) {
			r.updateStatus(ctx, kc)
		}

		kc.Spec = backup.Spec
		if !reflect.DeepEqual(current.Spec, kc.Spec) {
			r.updateSpec(ctx, kc)
		}

	}(&c)

	if isBeingDeleted(c) {
		logger.V(1).Info("Resource has deletiontimestamp. Will be deleted.")
		return r.reconcileDeletion(ctx, &c)
	}

	return r.reconcile(ctx, &c)
}

func (r *KindClusterReconciler) reconcile(ctx context.Context, c *infrastructurev1alpha1.KindCluster) (ctrl.Result, error) {
	logger := log.FromContext(ctx)
	if !controllerutil.ContainsFinalizer(c, ClusterFinalizer) {
		logger.Info("Adding finalizer")
		if err := r.addFinalizer(c); err != nil {
			return ctrl.Result{}, err
		}
	}

	status, err := r.Provider.GetStatus(ctx, c.Name)
	if err != nil {
		c.Status.UpdateForFailure(capierrors.CreateClusterError, err)
		return ctrl.Result{}, err
	}

	if !status.Exists {
		logger.Info("Cluster doesn't exist. Will create")
		if err = r.Provider.Create(ctx, c.Name); err != nil {
			c.Status.UpdateForFailure(capierrors.CreateClusterError, err)
			return ctrl.Result{}, err
		} else {
			return ctrl.Result{RequeueAfter: time.Second * 5}, nil
		}
	}

	if status.KubeConfig != "" {
		err = r.ensureKubeConfigSecret(ctx, c, status.KubeConfig)
		if err != nil {
			c.Status.UpdateForFailure(capierrors.CreateClusterError, err)
			return ctrl.Result{}, err
		}

		err = updateControlPlaneEndpoints(c, status.KubeConfig)
		if err != nil {
			c.Status.UpdateForFailure(capierrors.CreateClusterError, err)
			return ctrl.Result{}, err
		}
	}

	c.Status.Ready = status.IsReady
	c.Status.ClearFailure()
	if !c.Status.Ready {
		logger.V(1).Info("Cluster is not ready. Adding back to queue")
		return ctrl.Result{RequeueAfter: time.Second * 5}, nil
	}

	return ctrl.Result{}, nil
}

func updateControlPlaneEndpoints(c *infrastructurev1alpha1.KindCluster, kubeconfig string) error {
	config, err := clientcmd.Load([]byte(kubeconfig))
	if err != nil {
		return err
	}

	server := config.Clusters["kind-"+c.Name].Server
	server = strings.TrimPrefix(server, "https://")
	server = strings.TrimPrefix(server, "http://")
	parts := strings.Split(server, ":")
	port, err := strconv.ParseInt(parts[1], 10, 32)
	if err != nil {
		return err
	}

	c.Spec.ControlPlaneEndpoint.Host = parts[0]
	c.Spec.ControlPlaneEndpoint.Port = int32(port)

	return nil
}

func (r *KindClusterReconciler) reconcileDeletion(ctx context.Context, c *infrastructurev1alpha1.KindCluster) (ctrl.Result, error) {
	logger := log.FromContext(ctx)
	if !controllerutil.ContainsFinalizer(c, ClusterFinalizer) {
		logger.V(1).Info("Finalizer doesn't exist.")
		return ctrl.Result{}, nil
	}

	logger.Info("Deleting cluster")
	if err := r.Provider.Delete(ctx, c.Name); err != nil {
		c.Status.UpdateForFailure(capierrors.DeleteClusterError, err)
		return ctrl.Result{}, err
	}

	logger.V(1).Info("Ensuring cluster is deleted")
	status, err := r.Provider.GetStatus(ctx, c.Name)
	if err != nil {
		c.Status.UpdateForFailure(capierrors.DeleteClusterError, err)
		return ctrl.Result{}, err
	}

	c.Status.ClearFailure()
	if status.Exists {
		logger.V(1).Info("Cluster still exists after deletion. Will wait.")
		return ctrl.Result{RequeueAfter: time.Second * 5}, nil
	} else {
		logger.Info("Cluster doesn't exist anymore. Removing finalizer.")
		return ctrl.Result{}, r.removeFinalizer(c)

	}

}

// SetupWithManager sets up the controller with the Manager.
func (r *KindClusterReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&infrastructurev1alpha1.KindCluster{}).
		Owns(&v1.Secret{}).
		Complete(r)
}

func (r *KindClusterReconciler) addFinalizer(c *infrastructurev1alpha1.KindCluster) error {
	controllerutil.AddFinalizer(c, ClusterFinalizer)
	return r.Update(context.TODO(), c)
}

func (r *KindClusterReconciler) removeFinalizer(c *infrastructurev1alpha1.KindCluster) error {
	controllerutil.RemoveFinalizer(c, ClusterFinalizer)
	return r.Update(context.TODO(), c)
}

func (r *KindClusterReconciler) updateStatus(ctx context.Context, c *infrastructurev1alpha1.KindCluster) {
	logger := log.FromContext(ctx)
	logger.Info("Updating status", "status", c.Status)
	if err := r.Client.Status().Update(ctx, c); err != nil {
		logger.Error(err, "Error updating status")
	}
}

func (r *KindClusterReconciler) updateSpec(ctx context.Context, c *infrastructurev1alpha1.KindCluster) {
	logger := log.FromContext(ctx)
	logger.Info("Updating spec", "spec", c.Spec)
	if err := r.Client.Update(ctx, c); err != nil {
		logger.Error(err, "Error updating spec")
	}
}

func (r *KindClusterReconciler) ensureKubeConfigSecret(ctx context.Context, c *infrastructurev1alpha1.KindCluster, kubeconfig string) error {
	logger := log.FromContext(ctx)
	logger.V(1).Info("Getting kubeconfig secret")
	var secret v1.Secret
	err := r.Client.Get(ctx, types.NamespacedName{
		Name:      getKubeConfigSecretName(c),
		Namespace: c.Namespace,
	}, &secret)

	if err == nil {
		logger.V(1).Info("Kubeconfig secret already exists")
		return nil
	}

	if !errors.IsNotFound(err) {
		logger.Error(err, "error while fetching kubeconfig secret")
		return err
	}

	logger.Info("Kubeconfig secret doesn't exist. Will create")
	secret = v1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name:      getKubeConfigSecretName(c),
			Namespace: c.Namespace,
		},
		StringData: map[string]string{
			"value": kubeconfig,
		},
	}

	if err := controllerutil.SetControllerReference(c, &secret, r.Scheme); err != nil {
		logger.Error(err, "error while setting owner reference to kubeconfig secret")
		return err
	}

	if err := r.Client.Create(ctx, &secret); err != nil {
		logger.Error(err, "error while creating kubeconfig secret")
		return err
	}

	return nil
}

func getKubeConfigSecretName(c *infrastructurev1alpha1.KindCluster) string {
	return c.Name + kubeconfigSuffix
}

func isBeingDeleted(c infrastructurev1alpha1.KindCluster) bool {
	return !c.DeletionTimestamp.IsZero()
}

func ignoreIfNotFound(err error) error {
	if errors.IsNotFound(err) {
		return nil
	}
	return err
}
