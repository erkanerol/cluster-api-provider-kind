package controllers

import (
	"context"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	capierrors "sigs.k8s.io/cluster-api/errors"
	"time"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"k8s.io/apimachinery/pkg/types"

	infrastructurev1alpha1 "github.com/erkanerol/cluster-api-provider-kind/api/v1alpha1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var _ = Describe("KindCluster controller", func() {

	const (
		timeout  = time.Second * 30
		duration = time.Second * 3
		interval = time.Second * 1
	)

	Context("Happy Path for Cluster Lifecycle", func() {

		kindCluster := &infrastructurev1alpha1.KindCluster{
			TypeMeta: metav1.TypeMeta{
				APIVersion: "infrastructure.cluster.x-k8s.io/v1alpha1",
				Kind:       "KindCluster",
			},
			ObjectMeta: metav1.ObjectMeta{
				Name:      "test-cluster",
				Namespace: "test-1",
			},
		}

		createdKindCluster := &infrastructurev1alpha1.KindCluster{}
		kindClusterLookupKey := types.NamespacedName{Name: kindCluster.Name, Namespace: kindCluster.Namespace}

		createdKubeconfigSecret := &v1.Secret{}
		secretLookupKey := types.NamespacedName{Name: getKubeConfigSecretName(kindCluster), Namespace: kindCluster.Namespace}

		It("It should create a real kind cluster when KindCluster object is created", func() {

			ctx := context.Background()
			Expect(k8sClient.Create(ctx, &v1.Namespace{ObjectMeta: metav1.ObjectMeta{Name: kindCluster.Namespace}})).Should(Succeed())
			Expect(k8sClient.Create(ctx, kindCluster)).Should(Succeed())

			By("By adding finalizer when KindCluster is created")
			Eventually(func() []string {
				Expect(k8sClient.Get(ctx, kindClusterLookupKey, createdKindCluster)).Should(Succeed())
				return createdKindCluster.Finalizers
			}, timeout, interval).Should(ContainElement(ClusterFinalizer))

			By("By setting creating kubeconfig secret when kubeconfig is available")
			fakeProvider.MakeKubeConfigAvailable(createdKindCluster.Name)
			Eventually(func() bool {
				err := k8sClient.Get(ctx, secretLookupKey, createdKubeconfigSecret)
				if err != nil {
					return false
				}
				_, ok := createdKubeconfigSecret.Data["value"]
				return ok
			}, timeout, interval).Should(BeTrue())

			By("By setting control plane endpoints when kubeconfig is available")
			Eventually(func() bool {
				err := k8sClient.Get(ctx, kindClusterLookupKey, createdKindCluster)
				if err != nil {
					return false
				}

				if createdKindCluster.Spec.ControlPlaneEndpoint.Port != int32(6666) ||
					createdKindCluster.Spec.ControlPlaneEndpoint.Host != "1.2.3.4" {
					return false
				}
				return true
			}, timeout, interval).Should(BeTrue())

			By("By setting ready status as false when the cluster is not ready")
			Consistently(func() bool {
				Expect(k8sClient.Get(ctx, kindClusterLookupKey, createdKindCluster)).Should(Succeed())
				return createdKindCluster.Status.Ready
			}, duration, interval).Should(BeFalse())

			fakeProvider.MakeClusterReady(createdKindCluster.Name)
			By("By setting ready status as true when the cluster is ready")
			Eventually(func() bool {
				Expect(k8sClient.Get(ctx, kindClusterLookupKey, createdKindCluster))
				return createdKindCluster.Status.Ready
			}, timeout, interval).Should(BeTrue())
		})

		It("It should delete the real kind cluster when KindCluster object is deleted", func() {
			By("By keeping finalizer until the cluster is really deleted")
			fakeProvider.BlockDeletion(createdKindCluster.Name)
			Expect(k8sClient.Delete(ctx, createdKindCluster)).Should(Succeed())
			Consistently(func() []string {
				Expect(k8sClient.Get(ctx, kindClusterLookupKey, createdKindCluster)).Should(Succeed())
				return createdKindCluster.Finalizers
			}, duration, interval).Should(ContainElement(ClusterFinalizer))

			By("By removing finalizer and letting k8s to delete the object when the cluster is deleted")
			fakeProvider.ProcessDeletion(createdKindCluster.Name)
			Eventually(func() bool {
				err := k8sClient.Get(ctx, kindClusterLookupKey, createdKindCluster)
				return err != nil && errors.IsNotFound(err)
			}, timeout, interval).Should(BeTrue())

		})
	})

	Context("Problematic situations of Cluster Lifecycle", func() {

		kindCluster := &infrastructurev1alpha1.KindCluster{
			TypeMeta: metav1.TypeMeta{
				APIVersion: "infrastructure.cluster.x-k8s.io/v1alpha1",
				Kind:       "KindCluster",
			},
			ObjectMeta: metav1.ObjectMeta{
				Name:      "test-cluster-2",
				Namespace: "test-2",
			},
		}

		createdKindCluster := &infrastructurev1alpha1.KindCluster{}
		kindClusterLookupKey := types.NamespacedName{Name: kindCluster.Name, Namespace: kindCluster.Namespace}

		It("It should report issues when there is something wrong while creation", func() {

			fakeProvider.EnableErrorReturnForCreate(kindCluster.Name)

			ctx := context.Background()
			Expect(k8sClient.Create(ctx, &v1.Namespace{ObjectMeta: metav1.ObjectMeta{Name: kindCluster.Namespace}})).Should(Succeed())
			Expect(k8sClient.Create(ctx, kindCluster)).Should(Succeed())

			By("By setting failure message and reason when there is something wrong in creation")
			Eventually(func() bool {
				Expect(k8sClient.Get(ctx, kindClusterLookupKey, createdKindCluster)).Should(Succeed())
				return createdKindCluster.Status.FailureReason != nil &&
					*createdKindCluster.Status.FailureReason == capierrors.CreateClusterError &&
					createdKindCluster.Status.FailureMessage != nil &&
					*createdKindCluster.Status.FailureMessage == "fake create error"

			}, timeout, interval).Should(BeTrue())

			fakeProvider.DisableErrorReturnForCreate(kindCluster.Name)

			By("By cleaning when the problem is gone")
			fakeProvider.DisableErrorReturnForGetStatus(kindCluster.Name)
			Eventually(func() bool {
				Expect(k8sClient.Get(ctx, kindClusterLookupKey, createdKindCluster)).Should(Succeed())
				return createdKindCluster.Status.FailureReason == nil &&
					createdKindCluster.Status.FailureMessage == nil

			}, timeout, interval).Should(BeTrue())

			fakeProvider.EnableErrorReturnForGetStatus(kindCluster.Name)

			By("By setting failure message and reason when there is something wrong in status fetching")

			Eventually(func() bool {
				Expect(k8sClient.Get(ctx, kindClusterLookupKey, createdKindCluster)).Should(Succeed())
				return createdKindCluster.Status.FailureReason != nil &&
					*createdKindCluster.Status.FailureReason == capierrors.CreateClusterError &&
					createdKindCluster.Status.FailureMessage != nil &&
					*createdKindCluster.Status.FailureMessage == "fake get status error"

			}, timeout, interval).Should(BeTrue())

			By("By cleaning when the problem is gone")
			fakeProvider.DisableErrorReturnForGetStatus(kindCluster.Name)
			Eventually(func() bool {
				Expect(k8sClient.Get(ctx, kindClusterLookupKey, createdKindCluster)).Should(Succeed())
				return createdKindCluster.Status.FailureReason == nil &&
					createdKindCluster.Status.FailureMessage == nil

			}, timeout, interval).Should(BeTrue())
		})

		It("It should report issues when there is something wrong while deletion", func() {

			By("By setting failure message and reason when there is something wrong in deletion")
			fakeProvider.EnableErrorReturnForDelete(kindCluster.Name)
			Expect(k8sClient.Delete(ctx, createdKindCluster)).Should(Succeed())
			Eventually(func() bool {
				Expect(k8sClient.Get(ctx, kindClusterLookupKey, createdKindCluster)).Should(Succeed())
				return createdKindCluster.Status.FailureReason != nil &&
					*createdKindCluster.Status.FailureReason == capierrors.DeleteClusterError &&
					createdKindCluster.Status.FailureMessage != nil &&
					*createdKindCluster.Status.FailureMessage == "fake delete error"

			}, timeout, interval).Should(BeTrue())

			By("By cleaning when the problem is gone")
			fakeProvider.DisableErrorReturnForDelete(kindCluster.Name)
			fakeProvider.BlockDeletion(kindCluster.Name)
			Eventually(func() bool {
				Expect(k8sClient.Get(ctx, kindClusterLookupKey, createdKindCluster)).Should(Succeed())
				return createdKindCluster.Status.FailureReason == nil &&
					createdKindCluster.Status.FailureMessage == nil

			}, timeout, interval).Should(BeTrue())

			By("By removing finalizer and letting k8s to delete the object when there is no issue")
			fakeProvider.ProcessDeletion(kindCluster.Name)
			Eventually(func() bool {
				err := k8sClient.Get(ctx, kindClusterLookupKey, createdKindCluster)
				return err != nil && errors.IsNotFound(err)
			}, timeout, interval).Should(BeTrue())

		})
	})

})
