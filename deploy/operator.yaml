apiVersion: v1
kind: Namespace
metadata:
  labels:
    control-plane: controller-manager
  name: cluster-api-provider-kind-system
---
apiVersion: apiextensions.k8s.io/v1
kind: CustomResourceDefinition
metadata:
  annotations:
    controller-gen.kubebuilder.io/version: v0.7.0
  creationTimestamp: null
  name: kindclusters.infrastructure.cluster.x-k8s.io
spec:
  group: infrastructure.cluster.x-k8s.io
  names:
    kind: KindCluster
    listKind: KindClusterList
    plural: kindclusters
    singular: kindcluster
  scope: Namespaced
  versions:
  - name: v1alpha1
    schema:
      openAPIV3Schema:
        description: KindCluster is the Schema for the kindclusters API
        properties:
          apiVersion:
            description: 'APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#resources'
            type: string
          kind:
            description: 'Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#types-kinds'
            type: string
          metadata:
            type: object
          spec:
            description: KindClusterSpec defines the desired state of KindCluster
            properties:
              controlPlaneEndpoint:
                description: ControlPlaneEndpoint represents the endpoint used to communicate with the Kind Cluster
                properties:
                  host:
                    description: The hostname on which the API server is serving.
                    type: string
                  port:
                    description: The port on which the API server is serving.
                    format: int32
                    type: integer
                required:
                - host
                - port
                type: object
            type: object
          status:
            description: KindClusterStatus defines the observed state of KindCluster
            properties:
              failureMessage:
                description: FailureMessage indicates that there is a fatal problem reconciling the state, and will be set to a descriptive error message.
                type: string
              failureReason:
                description: FailureReason indicates that there is a fatal problem reconciling the state, and will be set to a token value suitable for programmatic interpretation.
                type: string
              ready:
                description: Ready denotes that the Kind cluster is ready.
                type: boolean
            type: object
        type: object
    served: true
    storage: true
    subresources:
      status: {}
status:
  acceptedNames:
    kind: ""
    plural: ""
  conditions: []
  storedVersions: []
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: cluster-api-provider-kind-controller-manager
  namespace: cluster-api-provider-kind-system
---
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  name: cluster-api-provider-kind-leader-election-role
  namespace: cluster-api-provider-kind-system
rules:
- apiGroups:
  - ""
  resources:
  - configmaps
  verbs:
  - get
  - list
  - watch
  - create
  - update
  - patch
  - delete
- apiGroups:
  - coordination.k8s.io
  resources:
  - leases
  verbs:
  - get
  - list
  - watch
  - create
  - update
  - patch
  - delete
- apiGroups:
  - ""
  resources:
  - events
  verbs:
  - create
  - patch
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  creationTimestamp: null
  name: cluster-api-provider-kind-manager-role
rules:
- apiGroups:
  - ""
  resources:
  - secrets
  verbs:
  - create
  - delete
  - get
  - list
  - patch
  - update
  - watch
- apiGroups:
  - infrastructure.cluster.x-k8s.io
  resources:
  - kindclusters
  verbs:
  - create
  - delete
  - get
  - list
  - patch
  - update
  - watch
- apiGroups:
  - infrastructure.cluster.x-k8s.io
  resources:
  - kindclusters/finalizers
  verbs:
  - update
- apiGroups:
  - infrastructure.cluster.x-k8s.io
  resources:
  - kindclusters/status
  verbs:
  - get
  - patch
  - update
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: cluster-api-provider-kind-metrics-reader
rules:
- nonResourceURLs:
  - /metrics
  verbs:
  - get
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: cluster-api-provider-kind-proxy-role
rules:
- apiGroups:
  - authentication.k8s.io
  resources:
  - tokenreviews
  verbs:
  - create
- apiGroups:
  - authorization.k8s.io
  resources:
  - subjectaccessreviews
  verbs:
  - create
---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: cluster-api-provider-kind-leader-election-rolebinding
  namespace: cluster-api-provider-kind-system
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: cluster-api-provider-kind-leader-election-role
subjects:
- kind: ServiceAccount
  name: cluster-api-provider-kind-controller-manager
  namespace: cluster-api-provider-kind-system
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: cluster-api-provider-kind-manager-rolebinding
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-api-provider-kind-manager-role
subjects:
- kind: ServiceAccount
  name: cluster-api-provider-kind-controller-manager
  namespace: cluster-api-provider-kind-system
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: cluster-api-provider-kind-proxy-rolebinding
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-api-provider-kind-proxy-role
subjects:
- kind: ServiceAccount
  name: cluster-api-provider-kind-controller-manager
  namespace: cluster-api-provider-kind-system
---
apiVersion: v1
data:
  controller_manager_config.yaml: |
    apiVersion: controller-runtime.sigs.k8s.io/v1alpha1
    kind: ControllerManagerConfig
    health:
      healthProbeBindAddress: :8081
    metrics:
      bindAddress: 127.0.0.1:8080
    webhook:
      port: 9443
    leaderElection:
      leaderElect: true
      resourceName: 4ae50012.cluster.x-k8s.io
kind: ConfigMap
metadata:
  name: cluster-api-provider-kind-manager-config
  namespace: cluster-api-provider-kind-system
---
apiVersion: v1
kind: Service
metadata:
  labels:
    control-plane: controller-manager
  name: cluster-api-provider-kind-controller-manager-metrics-service
  namespace: cluster-api-provider-kind-system
spec:
  ports:
  - name: https
    port: 8443
    protocol: TCP
    targetPort: https
  selector:
    control-plane: controller-manager
---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    control-plane: controller-manager
  name: cluster-api-provider-kind-controller-manager
  namespace: cluster-api-provider-kind-system
spec:
  replicas: 1
  selector:
    matchLabels:
      control-plane: controller-manager
  template:
    metadata:
      annotations:
        kubectl.kubernetes.io/default-container: manager
      labels:
        control-plane: controller-manager
    spec:
      containers:
      - args:
        - --secure-listen-address=0.0.0.0:8443
        - --upstream=http://127.0.0.1:8080/
        - --logtostderr=true
        - --v=10
        image: gcr.io/kubebuilder/kube-rbac-proxy:v0.8.0
        name: kube-rbac-proxy
        ports:
        - containerPort: 8443
          name: https
          protocol: TCP
      - args:
        - --health-probe-bind-address=:8081
        - --metrics-bind-address=127.0.0.1:8080
        - --leader-elect
        command:
        - /home/manager
        image: erkanerol/cluster-api-provider-kind:1.0
        livenessProbe:
          httpGet:
            path: /healthz
            port: 8081
          initialDelaySeconds: 15
          periodSeconds: 20
        name: manager
        readinessProbe:
          httpGet:
            path: /readyz
            port: 8081
          initialDelaySeconds: 5
          periodSeconds: 10
        resources:
          limits:
            cpu: 500m
            memory: 128Mi
          requests:
            cpu: 10m
            memory: 64Mi
        securityContext:
          allowPrivilegeEscalation: false
        volumeMounts:
        - mountPath: /var/run/docker.sock
          name: dockersock
      securityContext:
        runAsUser: 0
      serviceAccountName: cluster-api-provider-kind-controller-manager
      terminationGracePeriodSeconds: 10
      volumes:
      - hostPath:
          path: /var/run/docker.sock
        name: dockersock
