package cluster

import "context"

type Provider interface {
	Create(ctx context.Context, clusterName string) error
	Delete(ctx context.Context, clusterName string) error
	GetStatus(ctx context.Context, clusterName string) (Status, error)
}

type Status struct {
	Exists     bool
	IsReady    bool
	KubeConfig string
}
