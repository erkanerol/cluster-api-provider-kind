package cluster

import (
	"context"
	"github.com/go-logr/logr"
	v1 "k8s.io/api/core/v1"
	"k8s.io/client-go/tools/clientcmd"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"
	kindcluster "sigs.k8s.io/kind/pkg/cluster"
	"time"
)

type KindProvider struct {
	Provider  *kindcluster.Provider
	InCluster bool
}

// Ensure KindProvider implements Provider
var _ Provider = &KindProvider{}

func (k *KindProvider) Create(ctx context.Context, clusterName string) error {
	logger := log.FromContext(ctx).WithName("KindProvider.Create")
	logger.Info("Creating the cluster", "time", time.Now())
	err := k.Provider.Create(clusterName, kindcluster.CreateWithKubeconfigPath("/tmp/kubeconfig-"+clusterName))
	logger.Info("Created the cluster", "time", time.Now())
	return err
}

func (k *KindProvider) Delete(ctx context.Context, clusterName string) error {
	logger := log.FromContext(ctx).WithName("KindProvider.Delete")
	logger.Info("Deleting the cluster")
	return k.Provider.Delete(clusterName, "")
}

func (k *KindProvider) GetStatus(ctx context.Context, clusterName string) (status Status, err error) {
	logger := log.FromContext(ctx).WithName("KindProvider.GetStatus")
	logger.V(1).Info("Check cluster exist")
	status.Exists, err = checkClusterExist(k.Provider, clusterName)
	if err != nil || !status.Exists {
		return
	}

	logger.V(1).Info("Get External KubeConfig cluster")
	status.KubeConfig, err = k.Provider.KubeConfig(clusterName, false)
	if err != nil {
		return
	}

	kubeConfigForHealthCheck := status.KubeConfig
	if k.InCluster {
		logger.V(1).Info("Get Internal KubeConfig cluster for health check")
		kubeConfigForHealthCheck, err = k.Provider.KubeConfig(clusterName, true)
		if err != nil {
			return
		}
	}

	logger.V(1).Info("Checking health of cluster by controlling nodes one by one", "kubeconfig", kubeConfigForHealthCheck)

	status.IsReady, err = checkClusterReady(kubeConfigForHealthCheck, logger)
	return
}

func checkClusterReady(kubeConfigForHealthCheck string, logger logr.Logger) (bool, error) {
	nodes, err := getNodes(kubeConfigForHealthCheck)
	if err != nil {
		return false, err
	}

	for _, node := range nodes.Items {
		for _, condition := range node.Status.Conditions {
			if condition.Type == "Ready" && condition.Status != v1.ConditionTrue {
				logger.V(1).Info("Node is not healthy", "node", node.Name)
				return false, nil
			}
		}
	}

	return true, nil
}

func getNodes(kubeConfigForHealthCheck string) (v1.NodeList, error) {
	restconfig, err := clientcmd.RESTConfigFromKubeConfig([]byte(kubeConfigForHealthCheck))
	if err != nil {
		return v1.NodeList{}, err
	}

	clusterClient, err := client.New(restconfig, client.Options{})
	if err != nil {
		return v1.NodeList{}, err
	}

	var nodes v1.NodeList
	err = clusterClient.List(context.TODO(), &nodes)
	if err != nil {
		return v1.NodeList{}, err
	}
	return nodes, nil
}

func checkClusterExist(provider *kindcluster.Provider, clusterName string) (bool, error) {
	clusters, err := provider.List()
	if err != nil {
		return false, err
	}

	for _, c := range clusters {
		if c == clusterName {
			return true, nil
		}
	}

	return false, nil
}
