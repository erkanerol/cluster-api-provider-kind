# Cluster API Provider Kind (CAPK)

Kubernetes-native declarative infrastructure for [kind](https://kind.sigs.k8s.io/).

An operator which allows you to use a kind cluster as management-cluster and create new kind clusters with `KindCluster` custom resources.


![alt text](./docs/architecture.png)

# Prerequisites

- Docker engine must be running.
- `docker` binary must be in your PATH
- `kind` binary must be in your PATH 

# Usage

- Create a kind cluster which mounts docker socket of your host machine
```shell
$ cat > kind-cluster-with-extramounts.yaml <<EOF
kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
nodes:
- role: control-plane
  extraMounts:
    - hostPath: /var/run/docker.sock
      containerPath: /var/run/docker.sock
EOF
```

```shell
$ kind create cluster --name management-cluster --config kind-cluster-with-extramounts.yaml
```

- Wait until management-cluster is ready
```shell
$ kubectl get nodes -w
NAME                               STATUS   ROLES                  AGE   VERSION
management-cluster-control-plane   Ready    control-plane,master   50s   v1.21.1
```

- Deploy cluster-api-provider-kind operator
```shell
$ kubectl apply -f https://gitlab.com/erkanerol/cluster-api-provider-kind/-/raw/main/deploy/operator.yaml
```

- Wait until the operator is ready
```shell
$ kubectl -n cluster-api-provider-kind-system  get pods -w
NAME                                                            READY   STATUS    RESTARTS   AGE
cluster-api-provider-kind-controller-manager-764d64ccbf-pfzcp   2/2     Running   0          95s
```

- Create a `KindCluster`
```shell
$ kubectl apply -f https://gitlab.com/erkanerol/cluster-api-provider-kind/-/raw/main/config/samples/infrastructure_v1alpha1_kindcluster.yaml
```

- Wait until it is ready
```shell
$ kubectl get kindcluster workload-cluster-1  -o jsonpath='{.status}' -w
{"ready":true}
```

- Verify the cluster is created
```shell
$ kind get clusters
management-cluster
workload-cluster-1
```

- Get kubeconfig file of new cluster
```shell
$ kubectl  get secret  workload-cluster-1-kubeconfig  -o jsonpath='{.data .value}' |base64 -d > /tmp/kubeconfig
```

- Access the new cluster
```shell
$ KUBECONFIG=/tmp/kubeconfig kubectl get nodes
NAME                               STATUS   ROLES                  AGE     VERSION
workload-cluster-1-control-plane   Ready    control-plane,master   2m43s   v1.21.1
```
